
# Projet Web3D : Scène de crime

Application web d'une representation d'une 
scène de crime réalisée dans le cadre d'un projet scolaire utilisant la technologie X3DOM.
Voici une [vidéo de démonstration](https://www.canva.com/design/DAE4sqVLWsY/0uUN7KJkiBn0X4hqTKJT8Q/watch?utm_content=DAE4sqVLWsY&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink) disponible sur Canvas.


## Installation

Vous avez besoin d'une connection internet pour afficher correctement les pages.

```sh
git clone https://gitlab.com/syl.alf999/web3d-project.git
cd web3d-project
```

### Installer depuis un serveur existant [Nginx, Wamp, etc..]
1. Déplacer le repertoire web3d-project dans votre serveur
2. Démarrer le serveur

### Installer avec docker-compose
```sh
docker-compose up
```
Lancez notre application sur localhost:80
