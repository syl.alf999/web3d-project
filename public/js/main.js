class Evidence {
    constructor(title, description, location, imgFile, x3dFile) {
        this.title = title;
        this.description = description;
        this.location = location;
        this.imgFile = imgFile;
        this.x3dFile = x3dFile;
        this.hasBeenFound = false;
    }
}

// -- CONSTANTS DEFINITION

// File paths
const x3D_FILE_PATH = 'x3dModels/';
const IMG_FILE_PATH = 'public/img/';

const currentViewPoint = document.getElementById('currentViewPoint');

const toEntrance = document.getElementById('goToEntrance');
const toLivingRoom = document.getElementById('goToLR');
const toBedroom = document.getElementById('goToBedroom');
const toBathroom = document.getElementById('goToBathroom');

const infoAboutClickedObject = document.getElementById('infoObjectClicked');

const evidenceBox = document.getElementById('evidenceBox');
const localisationModal = document.getElementById('modal_body');
const localisationAlert = document.getElementById('localisationMsgAlert');
const localisationForm = document.querySelector('#exampleModal form');
const storyTab = document.getElementById('nav-story-tab');

const evidenceTitle = document.getElementById('evidence_title');
const evidenceDescription = document.getElementById('evidence_description');

const gallery = document.getElementById('gallery');

let storyUnlocked = false;
const rooms = ['Entrée', 'Salon', 'Salle de bain', 'Chambre'];
const evidences = [
    new Evidence('Statue', `La statue ayant infligée le coup fatal sur la tête de la victime.`, 'Chambre', 'statue.png', 'statue.x3d'),
    new Evidence('Passeport', `La victime est revenu du Japon la veille du meurtre le 30/07.` , 'Salon', 'passport.png', 'passport.x3d'),
    new Evidence('TV', `La TV ne fonctionnait pas le jour du meurtre.`, 'Salon', 'TV.png', 'TV.x3d'),
    new Evidence('Telephone', `La victime possède un téléphone fixe sans fil. Les téléphones filaires fonctionnent même lors d'une coupure`, 'Entrée', 'phone.png', 'phone.x3d')
];

toEntrance.addEventListener('click', () => toRoom(-5.24049, 8.10814));
toBathroom.addEventListener('click', () => toRoom(-0.66755, 7.76540));
toLivingRoom.addEventListener('click', () => toRoom(-12.02014, 8.00437));
toBedroom.addEventListener('click', () => toRoom(-5.25719, 2.78094));

/**
 * Call init functions when DOM is ready
 */
document.addEventListener('DOMContentLoaded', function () {
    createEvidenceCatalogueList(evidences);

    evidences.forEach((evidence) => {
        createRadioButtonGroups(evidence);
    });

    document.querySelector('#exampleModal form').addEventListener('submit', submitLocalisationForm);

    // Display the first model information
    gallery.getElementsByTagName('td')[0].click();
});

// 1ST PART -- MAP AND 3D MODEL INTERACTIONS 

/**
 * Retrieve position clicked from the map
 * and redirect the user in the interactive 3D model
 * @param event 
 */
function getPositionFromMap(event) {
    var coordinates = event.hitPnt;
    //console.log(event);
    //console.log(coordinates);
    // 0 : coordX, 1 coordY, 2 : coordZ
    const position = `${coordinates[0]} 1.5 ${coordinates[2]}`;
    currentViewPoint.setAttribute('position', position);
    currentViewPoint.setAttribute('orientation', '0 0 0 0');
    currentViewPoint.setAttribute('set_bind', true);
}

/**
 * Display information about a clicked object
 * @param {*} event 
 */
function getInformation(event) {
    //console.log(event.target.parentElement);
    //console.log(event.target.parentElement.attributes['DEF'].textContent);
    const objectClicked = event.target.parentElement.attributes['DEF'].textContent;
    const evidence = evidences.find((evidence) => `group_ME_${evidence.title}` === objectClicked);

    if (evidence !== undefined) {
        infoAboutClickedObject.style.display = "block";
        infoAboutClickedObject.querySelector('h5').textContent = evidence.title;
        infoAboutClickedObject.querySelector('p').textContent = evidence.description;
    }
}

/**
 * Teleport the user to a predefined location
 * @param x
 * @param z 
 */
function toRoom(x, z) {
    currentViewPoint.setAttribute('position', `${x} 1.5 ${z}`);
    currentViewPoint.setAttribute('orientation', '0 0 0 0');
    currentViewPoint.setAttribute('set_bind', true);
}


// 2ND PART -- ANALYZE PIECES OF EVIDENCE

/**
 * Create the DOM for the list of evidences
 * @param Evidence[] evidences 
 */
function createEvidenceCatalogueList(evidences) {

    const newRowElt = document.createElement('tr');

    evidences.forEach((evidence) => {
        let newTd = document.createElement('td');
        let img = document.createElement('img');
        img.setAttribute('src', IMG_FILE_PATH + evidence.imgFile);
        newTd.setAttribute('file', x3D_FILE_PATH + evidence.x3dFile);
        newTd.addEventListener('click', loadEvidenceModel);

        newTd.appendChild(img);
        newRowElt.appendChild(newTd);
    });

    gallery.appendChild(newRowElt);
}

/**
 * Load into x3d tags the model of a selected piece of evidence
 */
function loadEvidenceModel() {
    let x3DFile = this.getAttribute('file');

    const selectedEvidence = getEvidenceInfoFromX3DSelected(x3DFile);
    evidenceTitle.textContent = selectedEvidence.title;
    evidenceDescription.textContent = selectedEvidence.description;


    // Replace the x3d file in the context (if not already loaded)
    if (x3DFile != evidenceBox.getAttribute('url'))
        evidenceBox.setAttribute('url', x3DFile);
}

/**
 * Get evidence object to display information
 * @param string x3DFile 
 */
function getEvidenceInfoFromX3DSelected(x3DFile) {
    return evidences.find((evidence) => (x3D_FILE_PATH + evidence.x3dFile) === x3DFile);
}

/**
 * FORM HELPER
 * Create radio button groups for each evidence in the form
 * @param {*} evidence 
 */
function createRadioButtonGroups(evidence) {

    let div = document.createElement('div');
    div.setAttribute('class', 'btn-group');
    div.setAttribute('role', 'group');
    div.setAttribute('aria-label', 'Basic radio toggle button group');
    div.id = 'radioBtn_' + evidence.title;

    rooms.forEach((room) => {
        let label = document.createElement('label');
        label.setAttribute('class', 'btn btn-outline-primary');
        label.setAttribute('for', evidence.title + '_' + room);

        let input = document.createElement('input');
        input.setAttribute('type', 'radio');
        input.setAttribute('class', 'btn-check');
        input.setAttribute('name', evidence.title);
        input.setAttribute('value', room);
        input.id = evidence.title + '_' + room;

        label.appendChild(document.createTextNode(room));

        div.appendChild(input);
        div.appendChild(label);
    });

    let title = document.createElement('h4');
    title.appendChild(document.createTextNode(evidence.title + ' :'));

    localisationModal.appendChild(document.createElement('hr'));
    localisationModal.appendChild(title);
    localisationModal.appendChild(div);
}

/**
 * Handle form submission when sending the localisation of all pieces of evidence
 * Unlock history tab if all answers are correct
 * @param {*} event 
 */
function submitLocalisationForm(event) {
    event.preventDefault();

    let formData = new FormData(localisationForm);
    let nbCorrectAnswers = 0;

    evidences.forEach((evidence) => {
        if (formData.get(evidence.title) == evidence.location)
            nbCorrectAnswers++;
    });

    if (storyUnlocked) {
        localisationAlert.setAttribute('class', 'alert alert-success');
        localisationAlert.textContent = 'L\'histoire a déjà été débloquée !';
    }
    else if (nbCorrectAnswers === evidences.length) {
        storyUnlocked = true;
        localisationAlert.setAttribute('class', 'alert alert-success');
        localisationAlert.textContent = 'Bravo ! Vous pouvez maintenant accéder à l\'onglet histoire !';
        storyTab.setAttribute('class', 'nav-link');
    }
    else {
        localisationAlert.setAttribute('class', 'alert alert-warning');
        localisationAlert.textContent = `Vous avez ${evidences.length - nbCorrectAnswers} erreur(s) !`;
    }
}